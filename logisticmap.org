#+OPTIONS: toc:nil tex:t H:6 date:t author:nil tags:nil num:nil
#+OPTIONS: html5-fancy:t
#+OPTIONS: html-link-use-abs-url:nil html-postamble:auto
#+OPTIONS: html-preamble:t html-scripts:t html-style:t
#+STARTUP: hideblocks
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport deprecated
#+PROPERTY: header-args :session logisticmap :results silent :exports both :eval never-export :comments link
#
#+LATEX_HEADER: \usepackage{natbib}
#+LATEX_HEADER: \usepackage[nomarkers,figuresonly]{endfloat}
#+COMMENT: When exported this, and uploading the image path needs to be set to start with root "/"
#+begin_export html
---
title: "Bokeh plotting in python"
date: 2018-12-27
---
#+end_export

* Introduction

There is something intriguing and beautiful about complexity arising from simplicity. A set of simple rules followed in a deterministic fashion can follow widely different trajectories and generate widely different patterns given small changes in initial conditions.

This document serves mainly as an exercise in visualization, however I felt I needed something interesting to visualize as well. Here i will use python3.6 and [[https://bokeh.pydata.org/en/latest/][bokeh]] to visualize the [[https://en.wikipedia.org/wiki/Logistic_map][logistic map]].

The logistic map is defined by a very simple looking iterative rule
\[x_{n+1}=r \cdot x_{n}(1+x_{n})\]

The logistic map has a single parameter \(r\) and needs an initial setting \(x_{0} \in [0,1)\). It maps the state \(x_{n} \rightarrow x_{n+1}\) for each iteration. The parameter \(r\) is interesting in the interval \([0,4]\).

* Setting up the environment

I start by seting up a python environment and import relevant packages. All packages needed are imported here (except for a few used only for testing alternatives below).
#+name: initiate-session
#+begin_src ipython :exports code :results silent :noweb yes
import sys
import os
import pandas as pd
import numpy as np
import scipy.stats as stats
pd.set_option('precision',3)
np.set_printoptions(precision=3, linewidth=1000)
import bokeh.plotting as bkp
from bokeh.transform import linear_cmap
import bokeh.palettes as pal
#+end_src

* Building the logisticmap

I opt for making a simple class to keep track of the state of the logistic map as well as its parameters.
#+name: logisticmap-iterator
#+begin_src ipython :exports code :results silent
class LogisticMap():
    __upper=4
    __lower=2.4

    def __init__(self, r=3.4, x0=0.5, n=1000):
        self.r = r%LogisticMap.__upper
        self.x0 = x0%1
        self.x = self.x0
        self.n = n

    def _itermap(self):
        x = self.x
        return self.r*x*(1-x)

    def generate(self):
        yield self.x
        for i in range(self.n):
            self.x = self._itermap()
            yield self.x

def wrap_iframe(fname, width="100%", height="400px"):

    s=f'<iframe src="{fname}" style="width:{width};height:{height};border:none;"></iframe>'
    return s
#+end_src

* Visualize the logistic map for a single set of initial values

First we need to setup the plot and initiate our map.
#+name: setup-plot
#+begin_src ipython :exports code :results silent
# import figure, output_file, show
logm = LogisticMap(x0=0.11, r=3.2, n=1000)

htmlfname = f"html/logisticmap_single_setting.html"
bkp.output_file(htmlfname)
fig = bkp.figure(plot_width=600,
                 plot_height=300,
                 y_range=[0,1],
                 x_range=[-0.5, min([logm.n, 60])],
                 tools="wheel_zoom,reset,pan,save")
#+end_src

and then we generate some data and plot it using bokeh. When this code is executed a html file will be generated with the file name =htmlfname=. This file can be opened in any decent browser to view the result. Here I wrote a simple function to generate an iframe that can be embedded in this exported html file.
#+name: viz-logistic-singlet
#+begin_src ipython :exports both :results html replace output
y = [i for i in logm.generate()]
x = range(len(y))
fig.line(x, y, line_width=2)
fig.circle(x, y, fill_color="white", size=8)
fig.toolbar.active_drag = None
fig.toolbar.autohide = True
fig.grid.minor_grid_line_dash = [2, 2]
fig.grid.minor_grid_line_color = 'navy'
fig.grid.minor_grid_line_alpha = 0.1
fig.axis.minor_tick_in = 3
fig.axis.minor_tick_out = 0
bkp.save(fig)
print(wrap_iframe(htmlfname, height='320px'))
#+end_src

#+RESULTS: viz-logistic-singlet
#+begin_export html
<iframe src="html/logisticmap_single_setting.html" style="width:100%;height:320px;border:none;"></iframe>
#+end_export

The result is a figure with some applied cosmetic configurations. The iframe width and height are estimated based on the figure size i choose in this instance.
Of course we could embed the figure in the page but I choose this solution for convenience.
Lets see if we can make the figure more interesting.

* Visualize the logistic map bifurcation diagram

Visualizing the bifurcation diagram requires us to generate a lot more data to cover the interesting parameter ranges. I also clip out the first half of the generated values before the map is stabilizing, as well as using only one initial value \(x_{0} = 0.5\), however we could very easily iterate over a fiew initial \(x_{0}\) values as well.
#+name: bifurcation-data
#+begin_src ipython :exports code :results silent
data = []
for r in np.arange(1,4,0.001):
    logm = LogisticMap(x0=0.5, r=r, n=1000)
    tmp = pd.DataFrame([i for i in logm.generate()][100:], columns=['x'])
    tmp['r'] = r
    # tmp.drop_duplicates()
    data.append(tmp)

data = pd.concat(data)
#+end_src

If we wish to plot the complete data-set we generated we need to plot src_ipython[:exports results :results replace output]{print(data.shape[0])} {{{results(=3604000=)}}} data points. This is a bit to much for the browser to handle comfortably. I therefore try the [[http://datashader.org/][datashader]] extension of the [[http://holoviews.org/][holoview]] package. A package specifically designed for big data visualization. It has a bokeh extension that I will use here.

Let's start by setting up basic stuff.
#+name: setup-datashader
#+begin_src ipython :exports both :results silent
import holoviews as hv
from holoviews import opts
import datashader as ds
from holoviews.operation.datashader import datashade, shade, dynspread, rasterize
from holoviews.operation import decimate

hv.extension('bokeh')
htmlfname = "html/logisticmap_bifurcation_diagram.html"
decimate.max_samples=1000
dynspread.max_px=20
dynspread.threshold=0.5
#+end_src

#+name: vis-logisticmap-bifurcation-diagram
#+begin_src ipython :exports both :results html replace output
points = hv.Points(data, kdims=['r', 'x']).opts(width=600, height=400)
bifurcation = datashade(points, x_sampling=0.01, y_sampling=0.01)
hv.save(shade(bifurcation), htmlfname)
print(wrap_iframe(htmlfname, height='300px'))
#+end_src

#+RESULTS: vis-logisticmap-bifurcation-diagram
#+begin_export html
<iframe src="html/logisticmap_bifurcation_diagram.html" style="width:100%;height:300px;border:none;"></iframe>
#+end_export
unfortunately this looks awful and the documentation for datashader and holoview is not very intuitive. At least if i want to use the bokeh features and not as an extension of regular matplotlib. I was hoping that holoview would integrate a bit better with bokeh. I will continue and try my own down-sample of data point to be able to plot a somewhat nice figure.

We need to figure out roughly how many data points the browser could handle. I'm going to guess \(\sim 100000\). For each \(r\)-parameter value I'm going to down-sample to a specific number of maximum =bins= iterating over a specific number of \(r\)-values and try to keep the number of data points below the wished number.
#+name: downsample-bifurcation
#+begin_src ipython :exports code :results silent
data = []
bins = 500
nr1 = 400
# nr2 = 500
x = 0.5
roundto = 4
# rvals = np.append(np.arange(2.4, 3.6, 1/nr1), np.arange(3.6, 4,1/nr2))
rvals = np.arange(2.4, 4, 1/nr1)
for r in rvals:
    logm = LogisticMap(x0=x, r=r, n=10000)
    dist = np.histogram(np.round([i for i in logm.generate()][5000:], roundto), bins, density=True)
    density = dist[0][dist[0] > 0]
    density = density/np.max(density)
    xvals = dist[1]
    __ = (np.roll(xvals,-1) - xvals)/2
    xvals = xvals[:-1] - __[:-1]
    xvals = xvals[dist[0] > 0]
    xvals[xvals < 0] = 0
    __ = pd.DataFrame([xvals, density]).T
    __.columns = ['x', 'density']
    __['r'] = r
    data.append(__)

data = pd.concat(data)
#+end_src
The nice thing about the above solution is that we can generate a wished number of "bins" and resolution. The resulting data-structure has a total of src_ipython[:exports results :results replace output]{print(data.shape[0])} {{{results(=72011=)}}} much less than we expect to be needed. One thing that I approximate here is the bin boarders of the =np.histogram= function. +I make the simplification that the lower boarder is the relevant one+ I use =np.roll= to estimate the center of the bin boarders and have to make sure it's not negative, i.e. in the cases where the boarder is in the range \([-,0]\) when the density is exactly 0.

#+name: viz-bifurcation-diagram
#+begin_src ipython :exports both :results html replace output
htmlfname = "html/logisticmap_bifurcation_diagram_fine.html"
bkp.output_file(htmlfname)
fig = bkp.figure(plot_width=600,
                 plot_height=400,
                 y_range=[0,1],
                 x_range=[logm._LogisticMap__lower, logm._LogisticMap__upper],
                 x_axis_label='r',
                 y_axis_label='x',
                 tools="wheel_zoom,reset,pan,save,box_zoom")

colors = linear_cmap('density', 'Plasma256', 1, 0)
data['alphas'] = data['density'] + 0.5
cr = fig.circle(x='r', y='x', source=data,
                line_color=None,
                fill_color=colors,
                alpha='alphas',
                size=1)

fig.toolbar.logo = None
fig.toolbar.active_drag = None
fig.toolbar.autohide = True
fig.grid.minor_grid_line_dash = [2, 2]
fig.grid.minor_grid_line_color = 'navy'
fig.grid.minor_grid_line_alpha = 0.1
fig.axis.minor_tick_in = 0
fig.axis.minor_tick_out = 0

bkp.save(fig)
print(wrap_iframe(htmlfname, height='420px'))
#+end_src

This figure looks much better. The big difference between this and datashader is that the dots are not constrained to pixels. This means that I can get more of a smooth look but loose coverage as I zoom in. One trick that is needed when using =source=data= is that I predefine a new field in the dataframe for my alpha values instead of inputing the values in the circle command directly. Alphas are only defined in the range \([0,1]\) but it looks like any larger value is truncated so I set a lower limit of alpha to 0.5 and let the plot do the rest. This adds a bit of gradient to the colors used in the plot. The same goes for size if one wishes to play with that, even tough an upper limit is not an issue there.
#+RESULTS: viz-bifurcation-diagram
#+begin_export html
<iframe src="html/logisticmap_bifurcation_diagram_fine.html" style="width:100%;height:420px;border:none;"></iframe>
#+end_export


* Summary

I have tested bokeh's basic plotting features for visualizing the [[https://en.wikipedia.org/wiki/Logistic_map][logistic map]]. Bokeh seem like a good alternative for interactive plots served over html. I'm not very versatile in js or css or working with servers but it seems that to be able to have more advanced features one needs to utilize the bokeh server feature. Something I will have to come back to in the future.

#+begin_export html
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
#+end_export
